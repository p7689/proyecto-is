/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import modelo.Direccion;
import modelo.Usuario;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import persistencia.NewHibernateUtil;

/**
 *
 * @author User
 */
public class DireccionDaoImple implements DireccionDao{

    @Override
    public List<Direccion> consultarDireccion() {
        Session session = null;
        List<Direccion> lstDireccion = null;
        try{
            session = NewHibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("From Direccion d inner join fetch d.usuario order by idDireccion"); 
            lstDireccion = query.list();    
        }
        catch(HibernateException e){
            System.out.println(e);
            
            
        }
        finally{
            if(session != null){
                session.close(); 
            }
        }
        return lstDireccion;
    }

    @Override
    public void agregarDetalle(Direccion direccion, int id_user) {
         Session session = null;
        try{
            session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query query = session.createSQLQuery("insert into direccion (id_usuario, descripcion, codigo_postal, ciudad) values (?, ?, ?, ?)"); 
            query.setInteger(0,id_user);
            query.setParameter(1,direccion.getDescripcion());
            query.setParameter(2,direccion.getCodigoPostal());
            query.setParameter(3,direccion.getCiudad());
            query.executeUpdate();
            
            session.getTransaction().commit();
               
        }
        catch(HibernateException e){
            System.out.println(e);
            
            
        }
       

    
    }
    
    
}
