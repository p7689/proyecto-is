/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import modelo.DetalleFactura;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Query;
import persistencia.NewHibernateUtil;

/**
 *
 * @author pazan mera
 */
public class DetalleDaoImple implements DetalleDao{

    @Override
    public List<DetalleFactura> consultarDetalle() {
        Session session = null;
        List<DetalleFactura> lstDetalle = null;
        try{
            session = NewHibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("From DetalleFactura d inner join fetch d.producto inner join fetch d.cabezeraFactura order by idDetalleFactura"); 
            lstDetalle = query.list();    
        }
        catch(HibernateException e){
            System.out.println(e);
            
            
        }
        finally{
            if(session != null){
                session.close(); 
            }
        }
        return lstDetalle;
    }

    @Override
    public void agregarDetalle(DetalleFactura detalleFactura, int id_prod) {
        Session session = null;
        try{
            session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query query = session.createSQLQuery("insert into detalle_factura (id_producto, total, iva, subtotal) values (?, ?, ?, ?)"); 
            query.setInteger(0,id_prod);
            query.setParameter(1,detalleFactura.getTotal());
            query.setParameter(2,detalleFactura.getIva());
            query.setParameter(3,detalleFactura.getSubtotal());
            query.executeUpdate();
            
            session.getTransaction().commit();
               
        }
        catch(HibernateException e){
            System.out.println(e);
            
            
        }
       

    }
    
}
