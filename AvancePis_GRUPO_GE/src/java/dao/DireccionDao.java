/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import modelo.Direccion;


/**
 *
 * @author User
 */
public interface DireccionDao {
    public List<Direccion> consultarDireccion();
    public void agregarDetalle(Direccion direccion, int id_user);
   

    
}
