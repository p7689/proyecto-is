/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import modelo.DetalleFactura;

/**
 *
 * @author pazan mera
 */
public interface DetalleDao {
    public List<DetalleFactura> consultarDetalle();
    public void agregarDetalle(DetalleFactura detalleFactura, int id_prod) ;

    
}
