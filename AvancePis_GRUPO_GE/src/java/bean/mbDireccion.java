/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import dao.DireccionDao;
import dao.DireccionDaoImple;
import dao.UsuarioDao;
import dao.UsuarioDaoImple;
import java.io.Serializable;
import java.util.List;
import javax.el.ELContext;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedProperty;
import javax.inject.Inject;
import modelo.Direccion;
import modelo.Usuario;

/**
 *
 * @author User
 */
@Named(value = "mbDireccion")
@ViewScoped
public class mbDireccion implements Serializable{

    Direccion obj_Direccion;
   

    @Inject
    mbSession firstBean ;
    
    public mbDireccion() {
        obj_Direccion = new Direccion(); 
        firstBean = new mbSession();
    }
    
    public List<Direccion> getLstDireccion(){
        DireccionDao obj_DireccionDao= new DireccionDaoImple();
        return obj_DireccionDao.consultarDireccion();
    }

    public Direccion getObj_Direccion() {
        return obj_Direccion;
    }

    public void setObj_Direccion(Direccion obj_Direccion) {
        this.obj_Direccion = obj_Direccion;
    }
    
    
    
    
    public void agregarDireccion(){ 
        DireccionDao obj_DireccionDao = new DireccionDaoImple();
        
//        Direccion d = new Direccion();
       // Usuario u = new Usuario();
        //u.setIdUsuario(mbse.usuario.getIdUsuario());
//        d.setCiudad(obj_Direccion.getCiudad());
//        d.setCodigoPostal(obj_Direccion.getCodigoPostal());
//        d.setDescripcion(obj_Direccion.getDescripcion());
        try{
          //  System.out.println("MI ID " + firstBean.getObj_Usuario().getIdUsuario());
            
            obj_DireccionDao.agregarDetalle(obj_Direccion, firstBean.getObj_Usuario().getIdUsuario());

       }
//           
      catch(Exception e){
           FacesContext.getCurrentInstance().addMessage(null,
                   new FacesMessage(FacesMessage.SEVERITY_ERROR,"",e.getMessage()));
        }
        //obj_Direccion= new Direccion();
        FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,"Msg","LOS DATOS INGRESADOS CORRECTAMENTE"));
    
    } 
    

    
}   
